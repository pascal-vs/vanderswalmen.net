---
title: "Uechi Ryu Nidan Black Belt"
description: "Description of the tests for the second Dan WUKF"
draft: false
weight: 2
belt:
  name: "Nidan"
  order: "2nd"
youtube-id: "QTxbc_oeRD0"
hojoundo_to_perform: 0
kyon: true
kata:
- id: "sanchin"
  name: "Sanchin & Sanchin Kitae"
  index: 1
  timecode: 449
- id: "kanshiwa"
  name: "Kanshiwa"
  index: 2
  timecode: 1821
- id: "kanshu"
  name: "Kanshu"
  index: 3
  timecode: 1937
- id: "seichin"
  name: "Seichin"
  index: 4
  timecode: 1993
- id: "seisan"
  name: "Seisan"
  index: 5
  timecode: 536
- id: "seiryu"
  name: "Seiryu"
  index: 6
  timecode: 2063
---
