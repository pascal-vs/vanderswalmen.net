---
title: "Uechi Ryu Shodan Black Belt"
description: "Description of the tests for the first Dan WUKF"
draft: false
weight: 1
belt:
  name: "Shodan"
  order: "1st"
youtube-id: "QTxbc_oeRD0"
hojoundo_to_perform: 3
hojoundo:
- id: "sokuto_geri"
  name: "Sokuto Geri"
  timecode: 1111
- id: "shomen_geri"
  name: "Shomen Geri"
  timecode: 1154
- id: "hajiki_uke_hiraken_tsuki"
  name: "Hajiki Uke Hiraken Tsuki"
  timecode: 1183
- id: "mawashi_tsuki"
  name: "Mawashi Tsuki"
  timecode: 1214
- id: "seiken_tsuki"
  name: "Seiken Tsuki"
  timecode: 1244
- id: "uke_shuto_uchi_ura_uchi_shoken_tsuki"
  name: "Uke Shuto Uchi Ura Uchi Shoken Tsuki"
  timecode: 1275
- id: "hiji_tsuki"
  name: "Hiji Tsuki"
  timecode: 1312
- id: "tenshin_zensoku_geri"
  name: "Tenshin Zensoku Geri"
  timecode: 1349
- id: "tenshin_kosoku_geri"
  name: "Tenshin Kosoku Geri"
  timecode: 1367
- id: "tenshin_shoken_tsuki"
  name: "Tenshin Shoken Tsuki"
  timecode: 1387
- id: "shomen_hajiki"
  name: "Shomen Hajiki"
  timecode: 1414
- id: "koi_no_shippo_uchi_tate_uchi"
  name: "Koi No Shippo Uchi Tate Uchi"
  timecode: 1448
- id: "koi_no_shippo_uchi_yoko_uchi"
  name: "Koi No Shippo Uchi Yoko Uchi"
  timecode: 1468
kyon: false
katalevel: 4
kata:
- id: "sanchin"
  name: "Sanchin & Sanchin Kitae"
  index: 1
  timecode: 449
- id: "kanshiwa"
  name: "Kanshiwa"
  index: 2
  timecode: 1821
- id: "kanshu"
  name: "Kanshu"
  index: 3
  timecode: 1937
- id: "seichin"
  name: "Seichin"
  index: 4
  timecode: 1993
- id: "seisan"
  name: "Seisan"
  index: 5
  timecode: 536
---
