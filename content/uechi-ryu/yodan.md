---
title: "Uechi Ryu Yodan Black Belt"
description: "Description of the tests for the fourth Dan WUKF"
draft: false
weight: 4
belt:
  name: "Yodan"
  order: "4th"
youtube-id: "QTxbc_oeRD0"
hojoundo_to_perform: 0
kyon: true
kata:
- id: "sanchin"
  name: "Sanchin & Sanchin Kitae"
  index: 1
  timecode: 449
- id: "kanshiwa"
  name: "Kanshiwa"
  index: 2
  timecode: 1821
- id: "kanshu"
  name: "Kanshu"
  index: 3
  timecode: 1937
- id: "seichin"
  name: "Seichin"
  index: 4
  timecode: 1993
- id: "seisan"
  name: "Seisan"
  index: 5
  timecode: 536
- id: "seiryu"
  name: "Seiryu"
  index: 6
  timecode: 2063
- id: "kanshin"
  name: "Kanshin"
  index: 7
  timecode: 2123
- id: "sanseiryu"
  name: "Sanseiryu"
  index: 8
  timecode: 712
---
