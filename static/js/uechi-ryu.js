function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

function selectHojoUndo() {
    const hojoUndoSelectedList = document.querySelectorAll(".hojoundo-selected");
    // reset display for all Hojo Undo
    for (let i = 0; i < hojoUndoSelectedList.length; i++) {
        hojoUndoSelectedList[i].className = "hojoundo-off";
    }
    const hojoUndoList = document.querySelectorAll(".hojoundo-off");
    hojoUndoArray = Array.from(hojoUndoList);
    // display maximum 3 Hojo Undo
    for (let i = 0; i < Math.min(3, hojoUndoList.length); i++) {
        chosen = getRandomInt(hojoUndoList.length - i);
        hojoUndoArray[chosen].className = "hojoundo-selected";
        hojoUndoArray.splice(chosen, 1);
    }
}

function selectKataShodan() {
    const kataListSelected = document.querySelectorAll(".kata-selected");
    // reset display for all Kata
    for (let i = 0; i < kataListSelected.length; i++) {
        kataListSelected[i].className = "kata-off";
    }
    const kataList = document.querySelectorAll(".kata-off");
    // display 1 Kata (1 -> 4)
    if (kataList.length > 0) {
        kataList[getRandomInt(kataList.length)].className = "kata-selected";
    }
}

function ShodanExam() {
    selectHojoUndo();
    selectKataShodan();
}

window.addEventListener('load', ShodanExam)
